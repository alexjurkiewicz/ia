// =============================================================================
// Copyright 2011-2019 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef ACTOR_ITEMS_HPP
#define ACTOR_ITEMS_HPP


namespace actor
{
class Actor;
}


namespace actor_items
{

void make_for_actor(actor::Actor& actor);

} // actors_items

#endif // ACTOR_ITEMS_HPP
